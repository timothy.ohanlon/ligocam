#!/usr/bin/env python
# Author: Benjamin Mannix (2024)

"""
This script is desinged to monitor the results of LigoCAM runs and alert designated
email receipients in the event that an issue is discovered. 

Inputs:
Config file to get run directory, email receipients, and channel list of the IFO subsystem

Outputs:
Emails the designated receipients if an issue is found with the current LigoCAM run

"""


import os
from argparse import ArgumentParser
try:
    from configparser import ConfigParser
except ImportError:  # python 2.x
    from ConfigParser import ConfigParser

import gwpy.time
from datetime import datetime

import smtplib
from email.mime.text import MIMEText

import pandas as pd

__author__ = 'Benjamin Mannix <benjaminrobert.mannix@ligo.org>'

# =============================================================================

# Argument parsing from command line
argparser = ArgumentParser()
argparser.add_argument('config_file', help="LigoCAM configuration file.")
args = argparser.parse_args()
config_file = args.config_file

#Config parsing from config file
config = ConfigParser()
config.read(config_file)
ifo = config.get('Run', 'ifo')
subsystem = config.get('Run', 'subsystem')
email_to_1 = config.get('Email', 'email_1')
email_to_2 = config.get('Email', 'email_2')
email_from = config.get('Email', 'from')
email_replyto = config.get('Email', 'reply_to')
out_dir = config.get('Paths', 'out_dir')
channel_list = config.get('Paths', 'channel_list')

# we want to monitor status of latest results file 
results = out_dir +  '/results/results_current.txt'


def write_email(ifo, subsystem, results, channel_list):
    """
    Runs through various checks of the results file and outputs:
    The text for the email body, the subject of the email, and 
    a boolean to determine if an email is even needed/if there is an issue
    """
    
    # if there are no problems (HA!) then no email needs to be sent
    email_needed = False
    
    current_time = gwpy.time.tconvert('now')
    current_time_UTC = gwpy.time.tconvert(current_time)
    
    text = 'Time this report was generated: \n' + str(current_time_UTC) + '\n \n'
    subject = '[LigoCAM] Report {} for {} {}: '.format(current_time_UTC.date(), ifo, subsystem)
    
    # if results_current.txt is empty
    if os.path.getsize(results) == 0:
        email_needed = True
        subject = subject + 'No results generated!'
        text = text + 'Current results file is empty. Check condor logs for potential NDS/permissions issues.' 
        
        return text, subject, email_needed
    
    time_modified = os.path.getmtime(results) 
    time_modified = datetime.utcfromtimestamp(time_modified).strftime('%Y-%m-%d %H:%M:%S')
    time_modified = gwpy.time.tconvert(time_modified)

    # now check if it's been more than a day
    day_length = 24*60*60
    time_diff_modified = current_time - time_modified
    
    if time_diff_modified > day_length:
        email_needed = True
        subject = subject + 'Results haven\'t been generated in the last day'
        text = text + 'The current results file hasn\'t been modified in at least 24 hours. Check if LigoCAM is running' 
        
        return text, subject, email_needed
    
    
    # check if all channels made it into the results file

    # The text file has no headings, so I'll add them
    column_labels = ['Channel','0.03-0.1','0.1-0.3','0.3-1','1-3','3-10','10-30','30-100',
                     '100-300', '300-1000', '1000-3000','3000-10000','BLRMS Change?',
                     'BLRMS Time','Problem?', 'Problem Time']

    # read in results file and channel list
    results_df =  pd.read_csv(results, names = column_labels)
    channel_list = pd.read_csv(channel_list, header = None)

    # find if any channels 
    channels_not_in_results = channel_list[~channel_list[0].isin(results_df['Channel']) ] 

    # find the usual problems
    disconn_channels = results_df.loc[results_df['Problem?'] == 'disconn']
    daqfail_channels = results_df.loc[results_df['Problem?'] == 'daqfail']
    saturate_channels = results_df.loc[results_df['Problem?'] == 'saturate']
    
    if len(channels_not_in_results) > 0: 
        email_needed = True
        text = text+ 'Channels that are not reported in the final results: \n' 
        for i,j in channels_not_in_results.iterrows():
            text = text+ j[0] + '\n'
            
        text = text + '\n'
    
    if len(disconn_channels) > 0: 
        email_needed = True
        text = text +  'Disconnected Channels: \n' 
        for i,j in disconn_channels.iterrows():
            text = text+ j[0] + '\n'
            
        text = text + '\n'

    if len(daqfail_channels) > 0: 
        email_needed = True
        text = text + 'Disconnected Channels: \n' 
        for i,j in daqfail_channels.iterrows():
            text = text+ j[0] + '\n'
            
        text = text + '\n'

    if len(saturate_channels) > 0: 
        email_needed = True
        text = text + 'Disconnected Channels: \n' 
        for i,j in saturate_channels.iterrows():
            text = text+ j[0] + '\n'
            
        text = text + '\n'

    subject = subject + 'Daily Problem Channels Report'
    
    return text, subject, email_needed



def send_email(text, subject, email_from, email_to, email_replyto):
    """
    Takes the outputs of write_email and the email receipients from the 
    config file to send the email
    """
    recipients = email_to.split(',')
    msg = MIMEText(text)
    msg['Subject'] = subject
    msg['From'] = email_from
    msg['To'] = ', '.join(recipients)
    msg.add_header('reply-to', email_replyto)
    s = smtplib.SMTP('localhost')
    s.sendmail(msg['From'], recipients, msg.as_string())
    s.quit()
    return


email_text, email_subject, email_needed = write_email(ifo, subsystem, results, channel_list)

if email_needed:
    send_email(email_text, email_subject, email_from, email_to_1, email_replyto)
#!/usr/bin/env python
# Copyright (C) 2013 Dipongkar Talukder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""
LIGO Channel Activity Monitor (LigoCAM) analyzes power spectra of auxiliary
channels and flags those that show signs of DAQ failure, disconnection, or
significant band-limited RMS changes. This script is the primary executable
run by ligocam-batch for each channel list. It performs the analysis and
exports the data to be post-processed by ligocam-post.
"""

from __future__ import division
import numpy as np
import os
import re
import glob
import time
import traceback
from argparse import ArgumentParser

try:
    from configparser import ConfigParser
except ImportError:  # python 2.x
    from ConfigParser import ConfigParser

from gwpy.time import from_gps, tconvert

import plugins.utils as lcutils
import plugins.refutils as lcrefutils
import plugins.analysis as lcanalysis
import plugins.alert as lcalert
import plugins.plot as lcplot

__author__ = 'Philippe Nguyen <philippe.nguyen@ligo.org>'

# =============================================================================

start_time = tconvert()
print("Process started at time {} ({})".
      format(start_time, tconvert(start_time)))

# Argument parsing
argparser = ArgumentParser()
argparser.add_argument('-c', '--config-file',
                       help="LigoCAM configuration file.")
argparser.add_argument('-t', '--current-time', type=int,
                       help="Current GPS time.")
argparser.add_argument('-r', '--reference-time', type=int,
                       help="Reference GPS time.")
argparser.add_argument('channel_list', help="Channel list.")
args = argparser.parse_args()
config_file = args.config_file
current_time = args.current_time
reference_time = args.reference_time
channel_list = args.channel_list

# Get strings for current date and time
current_time_utc = from_gps(current_time).strftime('%h %d %Y %H:%M:%S UTC')
year_month_str = from_gps(current_time).strftime('%Y_%m')

# Config parsing
config = ConfigParser()
config.read(config_file)
lookback_time = int(config.get('Run', 'lookback_time'))
duration = int(config.get('Run', 'duration'))
run_dir = config.get('Paths', 'run_dir')
out_dir = config.get('Paths', 'out_dir')
thresholds_config = config.get('Paths', 'thresholds')

# Directories
hist_dir = os.path.join(run_dir, 'history')
job_dir = os.path.join(run_dir, 'jobs', year_month_str, str(current_time))
results_dir = os.path.join(out_dir, 'results')
asd_dir = os.path.join(
    out_dir, 'images', 'ASD',  year_month_str, str(current_time))
ts_dir = os.path.join(
    out_dir, 'images', 'TS', year_month_str, str(current_time))
for d in [hist_dir, results_dir, asd_dir, ts_dir]:
    if not os.path.exists(d):
        os.makedirs(d)

# History file
hist_file = os.path.join(hist_dir, 'history.txt')

# Threshold dictionaries
thresholds = ConfigParser()
thresholds.read(thresholds_config)
blrms_thresholds = {
    key: float(value) for key, value in thresholds.items('BLRMS')}
daqfail_thresholds = {
    key: float(value) for key, value in thresholds.items('DAQFailure')}
disconn_thresholds = {
    key: float(value) for key, value in thresholds.items('Disconnection')}
disconn_thresholds['weak_mag_chans'] = [
    value for key, value in thresholds.items('Weak Magnetometers')]

# Get channel list
with open(channel_list, 'r') as f:
    channels = f.readlines()
channels = [c.rstrip() for c in channels]

for channel in channels:
    channel_name = channel.rstrip()
    print('\nChannel: {}'.format(channel_name))
    channel_filename = channel_name.rstrip('_DQ').replace(':', '_')

    # LOAD DATA
    t_fetch = time.time()
    try:
        timeseries, psd, freq = lcutils.get_data(
            channel_name, current_time, duration)
    except RuntimeError:
        print(traceback.format_exc())
        print("Failed to load current spectrum for.")
        continue
    ref_file_pattern = os.path.join(
        hist_dir, channel_filename + '_*.txt')
    try:
        # Search for reference file, raises IndexError if no files glob'd
        ref_file = glob.glob(ref_file_pattern)[0]
        # Fetch existing reference PSD, raises ValueError if bad format
        psd_ref_binned = np.loadtxt(ref_file)
    except (IndexError, ValueError, FileNotFoundError):
        print(traceback.format_exc())
        print('Could not find or use existing reference spectrum.')
        ref_file = None
        ref_time = reference_time
        ref_times = [ref_time - 3600 * i for i in range(12)]
        # Compute exponentially-averaged reference PSD
        print("Computing new reference spectrum.")
        try:
            psd_ref_binned = lcrefutils.get_ref_data(
                channel_name, ref_times, duration)
        except RuntimeError:
            print(traceback.format_exc())
            print("Failed to load reference spectrum for channel {}."
                  .format(channel_name))
            print("Skipping this channel.")
            continue
    else:
        # Figure out reference time from reference PSD file
        try:
            ref_time = int(re.findall('_([0-9]+).txt', ref_file)[0])
        except (IndexError, ValueError):
            ref_time = reference_time
            print("Could not determine reference time from filename {}."
                  .format(ref_file))
    time_diff = max(1, int(round((current_time - ref_time) / 3600)))
    dt_fetch = time.time() - t_fetch
    print("Fetch time: {:.1f} s.".format(dt_fetch))

    # ANALYSIS
    t_analysis = time.time()
    # Prepare binned data
    data_segs = lcanalysis.prep_data(freq, psd, psd_ref_binned, duration)
    freq_segs = data_segs['freq']
    psd_segs = data_segs['psd']
    freq_binned_segs = data_segs['freq_binned']
    psd_binned_segs = data_segs['psd_binned']
    psd_ref_binned_segs = data_segs['psd_ref_binned']
    if len(psd_binned_segs) != len(psd_ref_binned_segs):
        psd_ref_binned_segs = psd_binned_segs
    try:
        # This really should not fail! Skip this channel if it does
        assert len(psd_binned_segs) == len(psd_ref_binned_segs)
        assert all(
            len(psd_binned_segs[i]) == len(psd_ref_binned_segs[i])
            for i in range(len(psd_binned_segs))),\
        "Current and reference spectra are not the same length.\n"\
        "Skipping this channel."
    except AssertionError:
        print(traceback.format_exc())
        continue
    
    # Check for large BLRMS changes
    old_blrms_status, old_blrms_hour = lcalert.get_alert_status(
        hist_file, channel, blrms=True)
    blrms_changes, new_blrms_status = lcanalysis.check_blrms(
        channel, psd_binned_segs, psd_ref_binned_segs, blrms_thresholds)
    if new_blrms_status != 'ok' and new_blrms_status == old_blrms_status:
        new_blrms_hour = old_blrms_hour + time_diff
    elif new_blrms_status != 'ok':
        new_blrms_hour = 1
    else:
        new_blrms_hour = 0

    # Check for disconnection, DAQ failure, or hardware failure
    old_status, old_hour = lcalert.get_alert_status(hist_file, channel)
    new_status = lcanalysis.check_status(
        channel, timeseries, psd, duration,
        daqfail_thresholds, disconn_thresholds
    )
    if new_status != 'ok' and new_status == old_status:
        new_hour = old_hour + time_diff
    elif new_status != 'ok':
        new_hour = 1
    else:
        new_hour = 0
    dt_analysis = time.time() - t_analysis
    print("Analysis time: {:.1f} s.".format(dt_analysis))

    # OUTPUT DATA
    # Save results
    line = ([channel] + ['{:.3g}'.format(x) for x in blrms_changes] +
            [new_blrms_status, '{:.0f}'.format(new_blrms_hour),
             new_status, '{:.0f}'.format(new_hour)])
    channel_file = os.path.join(
        job_dir, 'results', '%s.txt' % channel.replace(':', '_'))
    with open(channel_file, 'w') as f:
        f.write(','.join(line) + '\n')
    # Save current PSD to reference if status is ok
    if new_status == 'ok' and new_blrms_status == 'ok':
        # Delete old reference file and save new one
        if ref_file is not None:
            os.remove(ref_file)
        new_ref_file = os.path.join(
            hist_dir, '{}_{}.txt'.format(channel_filename, current_time))
        psd_new = np.concatenate(psd_binned_segs, axis=0)
        if old_status == 'ok' and old_blrms_status == 'ok':
            psd_old = psd_ref_binned
            print("Adding current spectrum to reference spectrum.")
        else:
            psd_old = None
            print("Saving current spectrum as reference spectrum.")
        lcrefutils.save_new_ref(new_ref_file, psd_new, psd_old=psd_old)
    else:
        print("Based on the current channel status, the reference "
              "spectrum will not be updated.")
    # Plot spectra and time series
    asd_segs = [np.sqrt(seg) for seg in psd_segs]
    asd_binned_segs = [np.sqrt(seg) for seg in psd_binned_segs]
    asd_ref_binned_segs = [np.sqrt(seg) for seg in psd_ref_binned_segs]
    ts_file = os.path.join(ts_dir, channel.replace(':', '_') + '.png')
    asd_file = os.path.join(asd_dir, channel.replace(':', '_') + '.png')
    lcplot.ts_plot(
        channel, ts_file, timeseries, duration, current_time_utc)
    lcplot.asd_plot(
        channel, asd_file, freq_segs, freq_binned_segs, asd_segs,
        asd_binned_segs, asd_ref_binned_segs, current_time_utc, timeseries, duration)

end_time = tconvert()
print("\nProcess ended at time {} ({})"
      .format(end_time, tconvert(end_time)))
print("Total time elapsed: {} s".format(end_time - start_time))

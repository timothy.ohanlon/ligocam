#!/usr/bin/env python

# Copyright (C) 2013 Dipongkar Talukder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

""" This file is part of LIGO Channel Activity Monitor (LigoCAM)."""

from __future__ import division
import numpy as np

from . import utils as lcutils
from . import (SEGMENT_FREQS, ALPHA)

__author__ = 'Dipongkar Talukder <dipongkar.talukder@ligo.org>'

# =============================================================================


def get_ref_data(chan, times, dur, alpha=ALPHA):
    """
    Fetch reference PSDs and combine into one ref PSD.
    """

    p_list = []
    for t in times:
        try:
            _, p, _ = lcutils.get_data(
                chan, t, dur)
        except Exception:
            continue
        else:
            p = get_psd_ref_binned(p, dur)
            p_list.append(p)
    assert len(p_list) > 0, "All reference times failed to load."
    p_out = (p_list[0] + p_list[1]) / 2
    for p in p_list[2:]:
        p_out += alpha * (p - p_out)
    return p_out


def get_psd_ref_binned(psd, dur, segment_freqs=SEGMENT_FREQS):
    """
    Break reference PSD into segments and bin them separately.
    """

    seg_rngs = []
    for i, j in segment_freqs:
        li = int(np.ceil(i * dur))
        ui = int(np.ceil(j * dur))
        r = range(li, ui)
        seg_rngs.append(r)
    upper_bounds = [list(rng)[-1] for rng in seg_rngs]
    upper_bounds = [x for x in upper_bounds if x < len(psd)]
    end = min([len(psd), 10000 * dur])
    rng_last = range(upper_bounds[-1], end)
    seg_rngs = seg_rngs[:len(upper_bounds)] + [rng_last]
    segs = []
    for i, rng in enumerate(seg_rngs):
        seg = psd[rng]
        if i > 1:
            seg = lcutils.get_binned(seg, 10 ** (i // 2))
        segs.append(seg)
    psd_new = np.concatenate(segs, axis=0)
    return psd_new


def save_new_ref(filename, psd_new, psd_old=None, alpha=ALPHA):
    """
    Combine the current psd to the reference and save it for future use.
    """

    if (psd_old is not None) and (len(psd_old) == len(psd_new)):
        psd_new = psd_old + alpha * (psd_new - psd_old)
    np.savetxt(filename, psd_new)
    return

# Copyright (C) 2018 Philippe Nguyen
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

__author__ = "Philippe Nguyen (philippe.nguyen@ligo.org)"

import os
import re
import numpy as np

try:
    import configparser as ConfigParser
except ImportError:
    import ConfigParser

from gwpy.time import tconvert

# Column names for LigoCAM results files
columns = [
    'channel',
    '0.03-0.1',
    '0.1-0.3',
    '0.3-1',
    '1-3',
    '3-10',
    '10-30',
    '30-100',
    '100-300',
    '300-1000',
    '1000-3000',
    '3000-10000',
    'BLRMS excess',
    'BLRMS hour',
    'failure status',
    'failure hour'
]

#---------------------------------

def find_nearest_results(gps, pages_dir, results_dir):
    """
    Finds the nearest LigoCAM page to the given time.
    """
    pages = os.listdir(pages_dir)
    results = os.listdir(results_dir)
    page_pattern = '(?<=LigoCamHTML_)([0-9]+)(?=.html)'
    results_pattern = '(?<=results_)([0-9]+)(?=.txt)'
    page_times = []
    for p in pages:
        m = re.search(page_pattern, p)
        if m is not None:
            page_times.append(int(m.group()))
    results_times = []
    for r in results:
        m = re.search(results_pattern, r)
        if m is not None:
            results_times.append(int(m.group()))
    dt_pages = np.abs(gps - np.array(page_times))
    for i in np.argsort(dt_pages):
        t = page_times[i]
        results_idx = results_times.index(page_times[i])
        results_filename = os.path.join(
            results_dir, 'results_%s.txt' % page_times[i])
        statinfo = os.stat(results_filename)
        if statinfo.st_size > 1e3:
            return t, results_filename

#-------------------------------

def ligocam_dqr(gps, config_file):
    """
    Fetches nearest LigoCAM page to GPS time.
    """
    
    # Config parsing
    config = ConfigParser.ConfigParser()
    config.read(config_file)
    ifo = config.get('Run', 'ifo')
    subsystem = config.get('Run', 'subsystem')
    out_dir = config.get('Paths', 'out_dir')
    public_url = config.get('Paths', 'public_url')
    
    # Directories for results and pages
    utc = tconvert(gps)
    month_dir = '%s_%s' % (utc.year, utc.month)
    pages_dir = os.path.join(out_dir, 'pages', month_dir)
    results_dir = os.path.join(out_dir, 'results', 'old', month_dir)
    
    # Find nearest results page
    gps_near, results_filename = find_nearest_results(gps, pages_dir, results_dir)
    with open(results_filename) as f:
        data = [x.rstrip('\n').split(',') for x in f.readlines()]
    page_near = 'LigoCamHTML_%s.html' % gps_near
    page_url = os.path.join(public_url, 'pages', month_dir, page_near)

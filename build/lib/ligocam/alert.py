#!/usr/bin/env python

# Copyright (C) 2013 Dipongkar Talukder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import smtplib
from email.mime.text import MIMEText

__author__ = 'Philippe Nguyen <philippe.nguyen@ligo.org>'

# ================================================


def get_alert_status(history_file, channel, blrms=False):
    """
    Get alert status and hour for a single channel.
    """

    with open(history_file, 'r') as file:
        lines = [x.rstrip() for x in file.readlines()]
    status = 'ok'
    hour = 0
    for line in lines:
        if channel in line:
            if not blrms:
                status, hour = line.split(',')[1:3]
            else:
                status, hour = line.split(',')[3:5]
            break
    return status, int(float(hour))


def update_alert_status(history_file, channel, status, hour,
                        blrms, blrms_hour):
    """
    Update alert statuses and hours for a single channel.
    """

    new_line = ('{},{},{:.0f},{},{:.0f}\n'
                .format(channel, status, hour, blrms, blrms_hour))
    with open(history_file, 'r') as file:
        lines = [x for x in file.readlines()]
    for i, x in enumerate(lines):
        if channel in x:
            lines[i] = new_line
    with open(history_file, 'w') as file:
        file.writelines(lines)
    return


def find_bad_channels(history_file, alert_status, alert_hour):
    """
    Search for channels which have been disconnected
    or have had DAQ failures in the last 'alert_hour' hours.
    """

    bad_channels = []
    with open(history_file, 'r') as f:
        lines = [x.rstrip() for x in f.readlines()]
    for line in lines:
        s = line.split(',')
        if alert_status != 'excess':
            channel, status, hour = s[0:3]
        else:
            channel = s[0]
            status, hour = s[3:5]
        if (status == alert_status and
                int(float(hour)) == alert_hour):
            bad_channels.append(channel)
    return sorted(set(bad_channels))


def write_email(ifo, subsystem, url, alert_dict, alert_epoch):
    alertby = ' / '.join(
        [k for k, v in alert_dict.items() if len(v) > 0])
    subject = ('[LigoCAM] {} channels alert for {} {}'
               .format(alertby, ifo, subsystem))
    text = 'Alert epoch:\n{}\n\nURL:\n<{}>'.format(alert_epoch, url)
    for alert, channels in alert_dict.items():
        text += '\n\n  {} channels:\n  '.format(alert)
        text += '\n  '.join(channels)
    return text, subject


def send_email(text, subject, email_from, email_to, email_replyto):
    recipients = email_to.split(',')
    msg = MIMEText(text)
    msg['Subject'] = subject
    msg['From'] = email_from
    msg['To'] = ', '.join(recipients)
    msg.add_header('reply-to', email_replyto)
    s = smtplib.SMTP('localhost')
    s.sendmail(msg['From'], recipients, msg.as_string())
    s.quit()
    return


def reset_history(filename, channel):
    with open(filename, 'r') as f:
        lines = f.readlines()
    for i, line in enumerate(lines):
        if channel in line:
            lines[i] = '{},ok,0,ok,0\n'.format(channel)
    with open(filename, 'w') as f:
        f.writelines(lines)
    return

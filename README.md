# LIGO Channel Activity Monitor (ligocam).

`ligocam` is a diagnostic tool for monitoring auxiliary channels.
This include locating a malfunctioning channel, graphic information
of channel's time series and spectral data, and spectral change to understand
various band-limited environmental disturbances of non-astrophysical origin.

Currently, this is primarily used to quickly monitor the status of Physical Environmental Monitoring (PEM) channels. ligocam is run on an hourly basis where the results are displayed in a webpage format. More information can be found here: http://pem.ligo.org

## Conda environment setup and installation
To ensure dependencies are set up properly, ligocam is run in its own conda environment. Clone the repository:
```
git clone https://git.ligo.org/pem/ligocam.git
```
Open the new repository and create a conda environment from the config file:
```
cd ligocam
conda env create -f environment.yaml
```
Activate the conda environment and install `ligocam`:
```
conda activate ligocam
pip install .
```

## Setup
Each ifo and subsystem is run from its own config file. For first-time setup,
edit the configuration files located in `/etc/config/` for the desired ifo
and subsystem to your liking and feed it to `ligocam-setup` with the directory
containing the necessary static files, e.g.
```
ligocam-setup <config_file> <share_directory>
```
This will create a directory structure where outputs will be saved. (Note: the share_directory argument refers to the `share` folder found in this repository. This commands copies the contents of `share` folder to your results directory)

The config files contain all the necessary settings information for a ligocam run, and by default, are the settings used to run `ligocam` on the `detchar.ligocam` account. As of O4, only LHO_PEM and LLO_PEM are actively monitored.  

When you're ready to get `ligocam` running, your run directory should be
structured like this:
```
/
- config
    - <channel lists>
    - <config .ini files>
    - <thresholds.ini>
- LHO/LLO
    - <subsystem folders, e.g. PEM>
        - history
        - jobs
```
Your output directory should look like this:
```
- LHO/LLO
    - <subsystem folders, e.g. PEM>
        - calendars
        - css
        - images
        - pages
        - results
        - status
```
By default, the output and run directories are the same folder. 

## Running ligocam
Submit a batch of ligocam jobs to condor by running ligocam-batch with
the desired config file:
```
ligocam-batch <config_file>
```

Similar to the settings for run/output directory, the config file also contains settings for the subsystem run itself (notably the channel list and the thresholds to compare channels to)

On the `detchar.ligocam` account, `ligocam` is run on an hourly basis using a cron job. In the `etc` folder, `crontab.txt` has an example cron job text and `ligocam-cron.bash` is the bash script the example executes. 

## Resetting a channel's history
Acceptable reference PSDs and the number of hours a channel has been
disconnected or had a DAQ failure are all logged in the run directory.
After a channel has been fixed/modified or after a long break where ligocam is not running, sometimes it is useful to reset reference PSDs. You can reset an individual channel’s history or a list of channels with the command:
```
ligocam-reset <config_file> <channel_name or channel_txt_file>
```


## Information for development

The commands executed in the ligocam environment can all be found in the `bin` folder. The analysis scripts are contained in the `ligocam` folder. Scripts to create the calendar files along with a cron job example script are found in the `share` folder. 

### Common issues



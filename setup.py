#!/usr/bin/env python
#
# Project Librarian: Philippe Nguyen
#                    Graduate Student
#                    University of Oregon / LIGO Scientific Collaboration
#                    <philippe.nguyen@ligo.org>
#
# Copyright (C) 2013 Dipongkar Talukder
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from setuptools import (setup, find_packages)
import glob
import os

__version__ = '0.1'

# General info
DISTNAME = 'ligocam'
AUTHOR = 'Dipongkar Talukder'
AUTHOR_EMAIL = 'dipongkar.talukder@ligo.org'
MAINTAINER = 'Philippe Nguyen'
MAINTAINER_EMAIL = 'philippe.nguyen@ligo.org'
LICENSE = 'GPLv3'
DESCRIPTION = 'LIGO Channel Activity Monitor'
URL = 'http://pem.ligo.org'

# Requirements
install_requires = [
    'six>=1.16.0',
    'numpy>=1.21.4',
    'scipy>=1.7.3',
    'matplotlib>=3.5.1',
    'astropy>=5.0',
    'gwpy>=2.1.2',
    'lscsoft-glue>=2.0.0'
]

# Setup
packagenames = find_packages()
scripts = glob.glob(os.path.join('bin', '*'))

setup(
    name=DISTNAME,
    version=__version__,
    url=URL,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=MAINTAINER,
    maintainer_email=MAINTAINER_EMAIL,
    description=DESCRIPTION,
    license=LICENSE,
    packages=packagenames,
    include_package_data=True,
    scripts=scripts,
    install_requires=install_requires
)
